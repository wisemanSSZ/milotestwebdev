__author__ = 'wiseman'

import unittest
from best_before import gen_date

class TestBestBefore(unittest.TestCase):

    def test_year_is_four_digit_case(self):
        line = "2014/06/25"
        res = gen_date(line)
        expected_res = '2014-06-25'
        self.assertEqual(res, expected_res)

    def test_year_with_one_zero(self):
        line = '0/12/10'
        res = gen_date(line)
        expected_res = '2000-10-12'
        self.assertEqual(res, expected_res)

    def test_year_with_two_zeros(self):
        line = '00/5/07'
        res = gen_date(line)
        expected_res = '2000-05-07'
        self.assertEqual(res, expected_res)

    def test_year_three_digits(self):
        line = "345/3/2"
        res = gen_date(line)
        expected_res = '%s is illegal' % (line, )
        self.assertEqual(res, expected_res)

    def test_year_beyond_scope(self):
        line = '3000/04/7'
        res = gen_date(line)
        expected_res = '%s is illegal' % (line, )
        self.assertEqual(res, expected_res)

    def test_month_beyond_scope(self):
        line = '13/15/18'
        res = gen_date(line)
        expected_res = '%s is illegal' % (line, )
        self.assertEqual(res, expected_res)

    def test_multiple_variants_correct(self):
        line = '5/7/6'
        res = gen_date(line)
        expected_res = '2005-06-07'
        self.assertEqual(res, expected_res)

    def test_day_beyond_scope(self):
        line = '2035/5/32'
        res = gen_date(line)
        expected_res = '%s is illegal' % (line, )
        self.assertEqual(res, expected_res)


if __name__ == '__main__':
    unittest.main()