
__author__ = 'wiseman'

import sys
from sys import argv
import getopt
import itertools
import datetime

DAYS_IN_MONTH = {1: 31, 2: [28, 29],
                 3: 31, 4: 30,
                 5: 31, 6: 30,
                 7: 31, 8: 31,
                 9: 30, 10: 31,
                 11: 30, 12: 31}


def initialise():
    '''
    Function receives and parses arguments from user input.
    :return: input string from file
    '''
    found_i = False
    try:
        opts, args = getopt.getopt(argv[1:], "i:", ["ifile="])

    except getopt.GetoptError:
        print 'best_before.py -i <inputfile>'
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-i", "--ifile"):
            inputfile = arg
            found_i = True

    if not found_i:
        print("You must provide an input file \n"
              "best_before.py -i <inputfile>")
        sys.exit(2)

    opened_file = open(inputfile)
    line = opened_file.readline()

    return line


def clean_year(year):
    '''
    Performs basic logic on checking if the year is correct.
    :param year: candidate for a year
    :return: year, int in case there are no constraints errors.
    '''
    year_int = int(year)
    if 1 <= len(year) <= 2:
        year_int += 2000
    if 2000 <= year_int <= 2999:
        return year_int

    raise ValueError


def clean_month(month):
    '''
    Performs basic logic on checking if the month is correct.
    :param month: candidate for a month
    :return: month, int in case there are no constraints errors.
    '''
    month = int(month)
    if 1 <= month <= 12:
        return month

    raise ValueError


def clean_day(day, month, year):
    '''
    Performs basic logic on checking if the day is correct.
    :param day: candidate for a day
    :param month: cleansed month
    :param year: cleansed year
    :return:
    '''
    day = int(day)
    days_max = DAYS_IN_MONTH[month]
    if month == 2:
        if leapyr(year):
            days_max = 29
        else:
            days_max = 28
    if 1 <= day <= days_max:
        return day

    raise ValueError


def leapyr(n):
    '''
    Method checks if the year is leap
    :param n: year to check for leap
    :return: True if year is leap, False otherwise
    '''
    if n % 400 == 0:
        return True
    if n % 100 == 0:
        return False
    if n % 4 == 0:
        return True
    else:
        return False


def gen_date(line):
    '''
    Function is used to determine the smallest date.
    The algorithm is simple: function constructs all the
    possible combinations of dates and then compares them
    in order to determine the smallest.
    :param line: line with date in a given format
    :return: smallest date or notification of impossibility
    '''
    date_divided = line.split('/')
    date_candidates = []
    for item in itertools.permutations(date_divided, len(date_divided)):
        (year, month, day) = item
        try:
            cleant_year = clean_year(year)
            cleant_month = clean_month(month)
            cleant_day = clean_day(day, cleant_month, cleant_year)
            date_candidates.append("%d-%02d-%02d" % (cleant_year, cleant_month, cleant_day))
        except ValueError:
            continue
    if date_candidates:
        date_candidates = sorted(date_candidates, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
        smallest_date = date_candidates[0]
    else:
        smallest_date = "%s is illegal" % (line,)

    return smallest_date


if __name__ == '__main__':
    line = initialise()
    res = gen_date(line)
    print(res)