Milo Solutions Test Task.

12 Feb 2015

I. Introduction
This project was created as a part of application process to Milo Solutions.
Includes two parts:  Django-project named "django_task" and Python-script "best_before", located in
"python_task" folder.

II. Installation

In order to make the application work, you need to have Python 2 (2.6 or later) installed on your PC along
with Python utilities "pip" and "virtualenv". Instructions are oriented for a Unix-like OS. Minor changes
may occur to commands in other operating systems, eg Windows.

To launch the django app, you need:
1. cd to a directory of your choice (eg: cd ~/project)
2. git clone https://wisemanSSZ@bitbucket.org/wisemanSSZ/milotestwebdev.git
3. virtualenv milo-webdev --no-site-packages
   a) cd milo-webdev/bin/
   b) . activate
   c) cd ../../
4. cd milotestwebdev/django_task
5. pip install -r requirements.txt
6. python manage.py runserver
7. open http://127.0.0.1:8000/ in your browser
   Notes: Django app comes with a preinstalled db for demonstrative purposes. In case you'd
   like to clear the db, follow these steps:
   1. rm db.sqlite3
   2. python manage.py migrate
   A new database will be created then.

   To launch tests, please follow these instructions:
   1. python manage.py test fizzbuzz/tests

To launch a Python app, you need to follow Django app instructions up to the point 3 (including). Then
the instructions vary:
...
4. cd MiloTestPython/python_task
5. python best_before.py -i <inputfile>

   To launch tests, you need additional step:
   python test_bb.py


III. Release Notes

The project was completed according to the technical document
1. Created "best_before" script to solve a task for Python test.
2. Created project "django_task" to solve a task for Django test.
   a) Created logic required to add, remove, edit and view single user from
   the web site.
   b) Added additional task that allows to export CSV-file in excel-format.
   c) Created all the required user interface to present app to the end-user
   d) Added tests to automatically test app.