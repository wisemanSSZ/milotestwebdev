__author__ = 'wiseman'

from datetime import date
from django import template
from fizzbuzz.models import CustomUser
from fizzbuzz.some_logic import check_eligible, check_fizzbuzz

register = template.Library()


@register.tag(name="fizzbuzz")
def do_fizzbuzz(parser, token):
    try:
        tag_name, user_random = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires a single argument" % token.contents.split()[0])

    return FizzbuzzNode(user_random)


@register.tag(name="is_eligible")
def do_elligible(parser, token):
    try:
        tag_name, user_bd = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires a single argument" % token.contents.split()[0])

    return ElligibleNode(user_bd)


class FizzbuzzNode(template.Node):
    def __init__(self, user_random):
        self.user_random = template.Variable(user_random)

    def render(self, context):
        try:
            user_random = self.user_random.resolve(context)
        except template.VariableDoesNotExist:
            return ''

        res = check_fizzbuzz(user_random)
        return res


class ElligibleNode(template.Node):
    def __init__(self, user_bd):
        self.user_bd = template.Variable(user_bd)

    def render(self, context):
        try:
            user_bd = self.user_bd.resolve(context)
        except template.VariableDoesNotExist:
            return ''

        is_eligible = check_eligible(user_bd)
        return is_eligible


@register.filter(name='addcss')
def addcss(field, css):
    return field.as_widget(attrs={"class":css})





