__author__ = 'wiseman'

from django.contrib.staticfiles.storage import staticfiles_storage
from django.utils.safestring import mark_safe
from django import forms


class DatePickerWidget(forms.DateInput):
    '''
    Custom widget for datepicker. Uses jquery datepicker underneath.
    '''

    class Media:
        css = {
            'all': (staticfiles_storage.url('bootstrap/css/jquery-ui.min.css'),)
        }
        js = (
            staticfiles_storage.url('bootstrap/js/jquery-1.11.2.min.js'),
            staticfiles_storage.url('bootstrap/js/jquery-ui.min.js'),
        )

    def __init__(self, params='', attrs=None):
        self.params = params
        super(DatePickerWidget, self).__init__(attrs=attrs)

    def render(self, name, value, attrs=None):
        rendered = super(DatePickerWidget, self).render(name, value, attrs=attrs)
        return rendered + mark_safe(u'''<script type="text/javascript">
            $('#id_%s').datepicker({%s});
            </script>'''%(name, self.params,))