import csv
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from models import CustomUser
from forms import UserForm
from some_logic import check_eligible, check_fizzbuzz

def list_all(request):
    # Lists all users in db.
    all_users = CustomUser.objects.all()

    return render(request, "fizzbuzz/list_all.html",
                  {'all_users': all_users})


def view_user(request, user_id):
    # Lists single user in db.
    user = CustomUser.objects.get(id=user_id)

    return render(request, "fizzbuzz/view_one.html",
                  {'user': user})


def add_user(request):
    # Adds new user to db
    if request.method == 'POST':
        form = UserForm(request.POST)

        if form.is_valid():
            new_usrname = form.cleaned_data['username']
            new_brthdate = form.cleaned_data['birthday']
            CustomUser.objects.create_user(username=new_usrname,
                                           birthday=new_brthdate)
            return redirect("/")
    else:
        form = UserForm()

    return render(request, 'fizzbuzz/add_user.html',
                  {'form': form})


def remove_user(request, user_id):
    # Removes user from db
    user = get_object_or_404(CustomUser, id=user_id)
    user.delete()

    return redirect('/')


def edit_user(request, user_id):
    # Edits a single user.
    user = get_object_or_404(CustomUser, id=user_id)

    if request.method == 'POST':
        form = UserForm(request.POST, instance=user)

        if form.is_valid():
            user.save()
            return redirect("/")
    else:
        form = UserForm(instance=user)

    return render(request, 'fizzbuzz/edit_user.html',
                  {'form': form})


def csv_list(request):
    # Generates a report in a CSV-format.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=report.csv'

    writer = csv.writer(response, dialect=csv.excel)
    writer.writerow(['Username', 'Birthday', 'Eligible', 'Random Number', 'BizzFuzz'])

    users = CustomUser.objects.all()
    for user in users:
        eligible = check_eligible(user.birthday)
        fizzbuzz = check_fizzbuzz(user.random_number)

        writer.writerow([user.username, user.birthday,
                        eligible, user.random_number,
                        fizzbuzz])
    return response