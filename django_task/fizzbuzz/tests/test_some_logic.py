__author__ = 'wiseman'

from django.test import TestCase
import datetime
from datetime import date
from fizzbuzz.models import CustomUser
from fizzbuzz.some_logic import check_eligible, check_fizzbuzz

class TestCheckEligible(TestCase):

    def test_eligible_if_elder_then_13(self):
        date = datetime.date(2000, 10, 04)

        res = check_eligible(date)
        self.assertEqual(res, "allowed")

    def test_blocked_if_younger_then_13(self):
        date = datetime.date(2010, 10, 04)

        res = check_eligible(date)
        self.assertEqual(res, "blocked")

    def test_empty_if_none(self):
        date = None

        res = check_eligible(date)
        self.assertEqual(res, "unknown")


class TestCheckFizzBuzz(TestCase):

    def test_fizz(self):
        num = 10
        res = check_fizzbuzz(num)
        self.assertEqual(res, "Fuzz")

    def test_fizzbuzz(self):
        num = 30
        res = check_fizzbuzz(num)
        self.assertEqual(res, "BizzFuzz")

    def test_buzz(self):
        num = 18
        res = check_fizzbuzz(num)
        self.assertEqual(res, "Bizz")

