
__author__ = 'wiseman'

import os
import filecmp
from django.test import TestCase
from fizzbuzz.models import CustomUser

class TestListAllView(TestCase):

    def test_list_all_view_shows_all(self):
        CustomUser.objects.create(username="test1")
        CustomUser.objects.create(username="test2")

        response = self.client.get('/')

        self.assertContains(response, "test1")
        self.assertContains(response, "test2")


class TestShowOneView(TestCase):

    def test_view_one_shows_only_one(self):
        CustomUser.objects.create(username="test1")
        CustomUser.objects.create(username="test2")

        response = self.client.get('/user/1/')

        self.assertContains(response, "test1")
        self.assertNotContains(response, "test2")


class TestEditView(TestCase):

    def test_edit_redirect_to_root_on_success(self):
        CustomUser.objects.create(username="test1")

        response = self.client.post('/edit/1/', {"username":"test3",
                                                "birthday":"2015-02-03"})

        self.assertRedirects(response, '/')

    def test_edit_doesnt_redirect_on_error(self):
        CustomUser.objects.create(username="test1")
        CustomUser.objects.create(username="test3")

        response = self.client.post('/edit/1/', {"username":"test3",
                                                "birthday":"2015-02-03"})

        self.assertEqual(response.status_code, 200)

    def test_edit_updates_data(self):
        CustomUser.objects.create(username="test1")

        response = self.client.post('/edit/1/', {"username":"test3",
                                                "birthday":"2015-02-03"})

        self.assertEqual(CustomUser.objects.count(), 1)
        self.assertEqual(CustomUser.objects.first().username, "test3")


class TestAddView(TestCase):

    def test_add_redirects_to_root_on_success(self):
        response = self.client.post('/add/', {"username":"test1",
                                                "birthday":"2015-02-03"})

        self.assertRedirects(response, '/')

    def test_add_doesnt_redirect_on_error(self):
        CustomUser.objects.create(username="test1")
        CustomUser.objects.create(username="test3")

        response = self.client.post('/add/', {"username":"test3",
                                                "birthday":"2015-02-03"})

        self.assertEqual(response.status_code, 200)

    def test_add_creates_user(self):
        response = self.client.post('/add/', {"username":"test3",
                                                "birthday":"2015-02-03"})

        self.assertEqual(CustomUser.objects.count(), 1)
        self.assertEqual(CustomUser.objects.first().username, "test3")


class TestRemoveView(TestCase):

    def test_remove_redirects_to_root_on_success(self):
        CustomUser.objects.create(username="test1")

        response = self.client.post('/remove/1/', {"username":"test1",
                                                "birthday":"2015-02-03"})

        self.assertRedirects(response, '/')

    def test_remove_deletes_user(self):
        CustomUser.objects.create(username="test1")

        response = self.client.post('/remove/1/', {"username":"test3",
                                                "birthday":"2015-02-03"})

        self.assertEqual(CustomUser.objects.count(), 0)