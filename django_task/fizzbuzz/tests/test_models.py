import datetime

__author__ = 'wiseman'

from django.test import TestCase
from fizzbuzz.models import CustomUser

class TestModelCorrectness(TestCase):

    def test_model_is_saved(self):
        CustomUser.objects.create_user(username="test", birthday="2015-10-11")

        saved_user = CustomUser.objects.get(id=1)
        self.assertEqual(saved_user.username, "test")
        self.assertEqual(saved_user.birthday, datetime.date(2015, 10, 11))

    def test_random_number_is_generated(self):
        user_1 = CustomUser.objects.create_user(username="test1")

        self.assertIsNotNone(user_1.random_number)
