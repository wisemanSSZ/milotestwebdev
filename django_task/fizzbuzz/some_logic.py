__author__ = 'wiseman'

from datetime import date


def check_eligible(user_bd):
    '''
    Implements the logic of age check.
    Block user in case his age is below 14,
    allow access in the other case.
    :param user_bd: user birthday in datetime format
    :return: string with permission.
    '''
    if user_bd is not None:
        today = date.today()
        age = today.year - user_bd.year
        if age > 13:
            is_eligible = "allowed"
        else:
            is_eligible = "blocked"
        return is_eligible
    else:
        return "unknown"


def check_fizzbuzz(user_random):
    '''
    Implements the logic of fizzbuzz check.
    Print BizzFuzz, Bizz or Fuzz depending
    on the conditions.
    :param user_random: random number assigned to user
    :return: string with a comment
    '''
    if user_random is not None:
        if user_random % 5 == 0 and user_random % 3 == 0:
            return "BizzFuzz"
        if user_random % 3 == 0:
            return "Bizz"
        if user_random % 5 == 0:
            return "Fuzz"
    return ""
