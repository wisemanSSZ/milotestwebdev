__author__ = 'wiseman'

from django.forms.models import ModelForm
from models import CustomUser
from widgets import DatePickerWidget


class UserForm(ModelForm):

    class Meta:
        verbose_name = 'User'
        model = CustomUser
        fields = (
            'username',
            'birthday'
        )
        widgets = {'birthday': DatePickerWidget()}