import random
from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    '''
    CustomUser class extends the default Django Auth User.
    It has all the fields and permission setting of
    Django user, but adds two additional fields. Please, note
    that random number is assigned only one time at "save" operation.
    It should be all right since the correct way to create a user
    is to use Manager's create() method. Example:
    CustomUser.objects.create_user()
    '''
    birthday = models.DateField(
        verbose_name='Birthday',
        blank=True,
        null=True,
    )
    random_number = models.IntegerField(
        verbose_name='Random number',
        blank=True,
        null=True,
    )

    def save(self, *args, **kwargs):
        #This code only happens if the objects is
        #not in the database yet.
        if not self.pk:
            self.random_number = random.randint(1, 100)

        super(CustomUser, self).save(*args, **kwargs)