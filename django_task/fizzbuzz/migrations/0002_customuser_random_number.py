# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fizzbuzz', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='random_number',
            field=models.IntegerField(null=True, verbose_name=b'Random number', blank=True),
            preserve_default=True,
        ),
    ]
