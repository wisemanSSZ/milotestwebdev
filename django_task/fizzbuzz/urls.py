from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',
    url(r'^$', 'fizzbuzz.views.list_all', name='list_all'),
    url(r'^user/(?P<user_id>\d+)/$', "fizzbuzz.views.view_user", name="view_user"),
    url(r'^add/$', "fizzbuzz.views.add_user", name="add_user"),
    url(r'^remove/(?P<user_id>\d+)/$', 'fizzbuzz.views.remove_user', name="remove_user"),
    url(r'^edit/(?P<user_id>\d+)/$', 'fizzbuzz.views.edit_user', name="edit_user"),
    url(r'^report/$', 'fizzbuzz.views.csv_list', name="csv_list")
)
